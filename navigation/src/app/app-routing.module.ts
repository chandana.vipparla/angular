import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FirstchildComponent } from './firstchild/firstchild.component';
import { SecondchildComponent } from './secondchild/secondchild.component';
import { ThirdchildComponent } from './thirdchild/thirdchild.component';

const routes: Routes = 
[
  {path:'first', component:FirstchildComponent},
  {path:'second',component:SecondchildComponent},
  {path:'third',component:ThirdchildComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }