import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {Route, Router} from '@angular/router';


@Component({
  selector: 'app-thirdchild',
  templateUrl: './thirdchild.component.html',
  styleUrls: ['./thirdchild.component.css']
})
export class ThirdchildComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  backbutton()
  {
    // history.go(-1);
    location.back();
    // this.router.navigate(['/second']);
    // history.back();

  }
}
