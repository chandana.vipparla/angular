import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { MainModule } from './main/main.module';
import { FirstchildComponent } from './firstchild/firstchild.component';
import { SecondchildComponent } from './secondchild/secondchild.component';
import { ThirdchildComponent } from './thirdchild/thirdchild.component';

@NgModule({
  declarations: [
    AppComponent,
    FirstchildComponent,
    SecondchildComponent,
    ThirdchildComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // MainModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
