import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminChildComponent } from './admin-child/admin-child.component';
// import { AdminChildComponent } from './admin-child/admin-child.component';
import { AdminCompComponent } from './admin-comp/admin-comp.component';
import { AdminGuardGuard } from './admin-guard.guard';
import { AdminModule } from './admin/admin.module';
import { DepListComponent } from './dep-list/dep-list.component';
import { EmpListComponent } from './emp-list/emp-list.component';
import { UserModule } from './user/user.module';

const routes: Routes = 
[
  { path : 'departments',component :DepListComponent},
  { path : 'employees' ,component :EmpListComponent},
  { path :'admin' , loadChildren:()=>import('./admin/admin.module')
      .then(mod=>mod.AdminModule)},
  { path :'user' , loadChildren:()=>import('./user/user.module')
      .then(mod=>mod.UserModule),canLoad:[AdminGuardGuard]}, 
  { path :'admincomponent', component:AdminCompComponent, canActivate : [AdminGuardGuard],
                                                          canActivateChild : [AdminGuardGuard],
                                                          children:
                                                          [
                                                            { path:'adminchild',component:AdminChildComponent}
                                                          ],
                                                          canLoad : [AdminGuardGuard],
                                                          canDeactivate : [AdminGuardGuard],
                                                          resolve:{items:AdminGuardGuard}
                                                          }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [DepListComponent,EmpListComponent,AdminCompComponent] 