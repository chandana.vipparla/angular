import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AdminGuardGuard } from './admin-guard.guard';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminChildComponent } from './admin-child/admin-child.component';
@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    AdminChildComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [AdminGuardGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
