import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-admin-comp',
  templateUrl: './admin-comp.component.html',
  styleUrls: ['./admin-comp.component.css']
})
export class AdminCompComponent implements OnInit {

  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void 
  {
    console.log(this.route.snapshot.data);
  }

}
