import { state } from '@angular/animations';
import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanDeactivate, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree,Resolve,ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AdminCompComponent } from './admin-comp/admin-comp.component';
import { AdminModule } from './admin/admin.module';
import { UserModule } from './user/user.module';

@Injectable({
  providedIn: 'root'
})
export class AdminGuardGuard implements CanActivate, CanActivateChild, CanDeactivate<unknown>, CanLoad {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):Observable<boolean>|Promise<boolean>|boolean {
      console.log('Activated');
    return true;
  }
  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean {
      console.log('Child Activated');
    return true;
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): boolean {
      console.log('Loaded');
    return true;
  }
  canDeactivate(
    component: AdminCompComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): boolean {
      console.log('Deactivated');
    return window.confirm('do u want to deactivate?');
  }
  userObj = {
    userId:444,
    username:'chandana'
  }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.userObj;
    }
  
}


