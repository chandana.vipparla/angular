import { Component, ComponentFactoryResolver, ViewChild } from '@angular/core';
import {DynamicdirDirective} from './dynamicdir.directive';
import { DynamicComponent } from './dynamic/dynamic.component';
import { SerInjComponent } from './ser-inj/ser-inj.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent 
{
  title = 'angular-cli-playground';
  appliedclasses:string='font color';
  appliedbackground:boolean=false;
  appliedcolor:boolean=false;
  multiclass()
  {
    let c=
    {
      background:this.appliedbackground,
      color:this.appliedcolor
    }
    return c;
  }

  columnspan:number=3;
  firstname:string='Chandana';
  secondname:string="Venkatesh";
  lastname:string='Vipparla';
  active=true
  decorationstyle()
  {
    let s=
    {
      "font-style":"italic",
      "color":"pink",
    }
    return s;
  }

  birthday = new Date(1999, 3, 16);
  date = new Date();

  fname: string = "chandana"; 
  lname: string = "vipparla";
  show()
  {
   console.log(this.fname)  
   console.log(this.lname); 
  }

array=[1,2,3,4];

constructor(private ComponentFactoryResolver:ComponentFactoryResolver)
{}
@ViewChild(DynamicdirDirective) dynamic :DynamicdirDirective;

display()
  {
    const c=this.ComponentFactoryResolver.resolveComponentFactory(DynamicComponent);
    this.dynamic.vref.clear();
    this.dynamic.vref.createComponent(c);
  }

}
