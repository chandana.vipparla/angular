import { TestBed } from '@angular/core/testing';

import { NewserveService } from './newserve.service';

describe('NewserveService', () => {
  let service: NewserveService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NewserveService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
