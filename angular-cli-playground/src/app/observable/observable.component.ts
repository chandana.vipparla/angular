import { Component, OnInit } from '@angular/core';
import { observable, Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { ReplaySubject } from 'rxjs';
import { AsyncSubject } from 'rxjs';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.css']
})
export class ObservableComponent implements OnInit {

  status;
  data1;
  data2;
  constructor() { }

  ngOnInit(): void 
  {
    this.data1=new Observable(observer=>
    {
      setTimeout(()=>{observer.next("In progress")},2000)
      setTimeout(()=>{observer.next("In processing")},3000)
      setTimeout(()=>{observer.next("completed")},4000)
      setTimeout(()=>{observer.error()},4000)
      setTimeout(()=>{observer.complete()},4000)
      // it doesn't show
      setTimeout(()=>{observer.next("after complete")},8000)
    }).subscribe(val1=>this.status=val1)

    this.data2=new Observable(observer=>
      {
        console.log("in data 2")
      }).subscribe(val2=>this.status=val2)
      // this.data1.unsubscribe();
  }

}

// subject
let mySubject1 = new Subject();
mySubject1.subscribe({
  next: (value) => 
  console.log('First observer in subject:' + value)
});
mySubject1.subscribe({
  next: (value) => 
  console.log('Second observer in subject:' + value)
});
mySubject1.next('Hello');
mySubject1.next('Bye');

//behavioural subject
const mySubject2 = new BehaviorSubject('Hi');
mySubject2.subscribe({
  next: (value) => 
  console.log('First observer in b-sub:' + value)
});
mySubject2.next('Hello');
mySubject2.subscribe({
  next: (value) => 
  console.log('Second observer in b-sub:' + value)
});
mySubject2.next('Bye');

//replay subject
const mySubject3 = new ReplaySubject;
mySubject3.subscribe({
  next: (value) => 
  console.log('First observer in r-sub:' + value)
});
mySubject3.next('Hey');
mySubject3.next('Hi');
mySubject3.subscribe({
  next: (value) => 
  console.log('Second observer in r-sub:' + value)
});
mySubject3.next('Bye');

//async subject

const mySubject4 = new AsyncSubject();
mySubject4.subscribe({
  next: (value) =>
   console.log('First observer in async-sub:' + value)
});
mySubject4.next('Hey');
mySubject4.next('Hi');
mySubject4.subscribe({
  next: (value) => 
  console.log('Second observer in async-sub:' + value)
});
mySubject4.next('Bye');
mySubject4.complete();
