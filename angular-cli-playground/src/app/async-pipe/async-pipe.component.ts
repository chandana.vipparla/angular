import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-async-pipe',
  templateUrl: './async-pipe.component.html',
  styleUrls: ['./async-pipe.component.css']
})
export class AsyncPipeComponent implements OnInit {
  observable;
  k: any='chandana';
  constructor() { }

  ngOnInit(): void
  {
    this.observable = this.k.Observable.interval(1000);
  }

}
