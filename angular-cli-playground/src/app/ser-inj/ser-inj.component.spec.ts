import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SerInjComponent } from './ser-inj.component';

describe('SerInjComponent', () => {
  let component: SerInjComponent;
  let fixture: ComponentFixture<SerInjComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SerInjComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SerInjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
