import { Component, OnInit } from '@angular/core';
import { NewserveService } from '../newserve.service';

@Component({
  selector: 'app-ser-inj',
  templateUrl: './ser-inj.component.html',
  styleUrls: ['./ser-inj.component.css']
})
export class SerInjComponent implements OnInit {

s=[]

constructor(private service:NewserveService) 
{ 
  
}
ngOnInit(): void 
{
  this.s=this.service.getservices();
  console.log(this.s)
}
 

}
