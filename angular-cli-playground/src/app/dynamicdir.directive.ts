import { Directive,ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appDynamicdir]'
})
export class DynamicdirDirective {

  constructor(public vref:ViewContainerRef) { }

}
