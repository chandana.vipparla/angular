import { Component, OnInit } from '@angular/core';
import { NewserveService } from '../newserve.service';

@Component({
  selector: 'app-ser-inj2',
  templateUrl: './ser-inj2.component.html',
  styleUrls: ['./ser-inj2.component.css']
})
export class SerInj2Component implements OnInit {

  s=[]

  constructor(private service:NewserveService) 
  { 
    
  }
  ngOnInit(): void 
  {
    this.s=this.service.getservices();
    console.log(this.s)
  }

}
