import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SerInj2Component } from './ser-inj2.component';

describe('SerInj2Component', () => {
  let component: SerInj2Component;
  let fixture: ComponentFixture<SerInj2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SerInj2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SerInj2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
