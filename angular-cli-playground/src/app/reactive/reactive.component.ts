import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl } from '@angular/forms';
import { Validators } from  '@angular/forms';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {

  userprofileform=new FormGroup({
    firstname : new FormControl('',Validators.required),
    lastname : new FormControl(''),
    email : new FormControl(''),
  })

  countryList:country[] = [
    new country("1", "India"),
    new country('2', 'USA'),
    new country('3', 'England')
  ];
  onsubmit()
  {
    console.log(this.userprofileform.value);

    console.log(this.userprofileform.controls['firstname'].value);
  
    console.log(this.userprofileform.get('firstname').value);
  }
  constructor() { }

  ngOnInit(): void {
  }

}
export class country 
{
    id:string;
    name:string;
 
  constructor(id:string, name:string)
  {
    this.id=id;
    this.name=name;
  }
}
