import { Directive,Input,TemplateRef,ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appCustomstructural]'
})
export class CustomstructuralDirective {

  constructor(private templateRef: TemplateRef<any>,private viewContainer: ViewContainerRef) { }
  @Input() appCustomstructural;
  ngOnInit(){
  if(this.appCustomstructural)
  {
    this.viewContainer.createEmbeddedView(this.templateRef)
  }
}
}
