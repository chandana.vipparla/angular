import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChildComponent } from './child/child.component';
import { ParentComponent } from './parent/parent.component';
import { InPipe } from './in.pipe';
import { CustomDirective } from './custom.directive';
import { CustomstructuralDirective } from './customstructural.directive';
import { DynamicComponent } from './dynamic/dynamic.component';
import { DynamicdirDirective } from './dynamicdir.directive';
import { SerInjComponent } from './ser-inj/ser-inj.component';
import { NewserveService } from './newserve.service';
import { SerInj2Component } from './ser-inj2/ser-inj2.component';
import { ObservableComponent } from './observable/observable.component';
import { ReactiveComponent } from './reactive/reactive.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TemplateComponent } from './template/template.component';
import { ReactFormComponent } from './react-form/react-form.component';
import { AsyncPipeComponent } from './async-pipe/async-pipe.component';

@NgModule({
  declarations: [
    AppComponent,
    ChildComponent,
    ParentComponent,
    InPipe,
    CustomDirective,
    CustomstructuralDirective,
    DynamicComponent,
    DynamicdirDirective,
    SerInjComponent,
    SerInj2Component,
    ObservableComponent,
    ReactiveComponent,
    TemplateComponent,
    ReactFormComponent,
    AsyncPipeComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  
  providers: [NewserveService],
  bootstrap: [AppComponent,]
})
export class AppModule { }
