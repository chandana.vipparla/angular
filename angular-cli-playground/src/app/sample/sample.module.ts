import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChildComponent } from 'src/app/child/child.component';
import { ParentComponent } from 'src/app/parent/parent.component';


@NgModule({
  declarations: [ChildComponent,ParentComponent],
  imports: [
    CommonModule
  ],
  exports:[
    ChildComponent,
    ParentComponent
  ]
})
export class SampleModule { }
