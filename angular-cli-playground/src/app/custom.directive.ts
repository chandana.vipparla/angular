import { Directive } from '@angular/core';
import {ElementRef} from '@angular/core'
@Directive({
  selector: '[appCustom]'
})
export class CustomDirective {

  constructor(private eleRef: ElementRef) 
  {
    eleRef.nativeElement.style.color = 'blue';
  }
}
