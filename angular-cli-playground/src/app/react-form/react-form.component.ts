import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators, AbstractControl, ValidatorFn, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-react-form',
  templateUrl: './react-form.component.html',
  styleUrls: ['./react-form.component.css']
})
export class ReactFormComponent implements OnInit {

  loginForm : FormGroup;

  min = 10;
  max = 20;
  // constructor() { }
  // ngOnInit() {
  //     this.loginForm = new FormGroup({
  //         email: new FormControl(null, [Validators.required]),
  //         password: new FormControl(null, [Validators.required, Validators.maxLength(8)]),
  //         age: new FormControl(null, [ageRangeValidator(this.min, this.max)])
  //     });
  // }

  //using FormBuilder will look like
  constructor(private fb : FormBuilder){
 
  }
  ngOnInit() {
 
    this.loginForm = this.fb.group({
        email: [null, [Validators.required, Validators.minLength(4)]],
        password: [null, [Validators.required, Validators.maxLength(8)]],
        age: new FormControl(null, [ageRangeValidator(this.min, this.max)])
    })
}
  loginUser()
 {
    console.log(this.loginForm.status);
    console.log(this.loginForm.value);
  }
}
function ageRangeValidator(min: number, max: number): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (control.value !== undefined && (isNaN(control.value) || control.value < min || control.value > max)) {
          return { 'ageRange': true };
      }
      return null;
  };
}
