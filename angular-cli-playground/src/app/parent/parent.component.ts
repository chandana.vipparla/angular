import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { ChildComponent } from '../child/child.component';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-parent',
  // templateUrl: './parent.component.html',
  template:
  `<p>{{message}}</p>
  <button (click)="aa()">kill</button>
  <div *ngIf="v">
  <app-child (childEvent)="message=$event"  [data]="master"></app-child>
  </div>`,
  styleUrls: ['./parent.component.css']
})
export class ParentComponent {
  master = 'parent message!';
  message:string;
  v:boolean=true;
  // temp="chandu";
  constructor()
  {}
  aa()
  {
    this.v=!this.v;
  }
  ngOnChanges()
  {
    console.log("parent changed");
  }
  ngOnInit():void
  {
    console.log("parent initialization");
    // console.log(this.temp);
  }
  ngDoCheck()
  {
    console.log("parent checked");
  }
  ngAfterContentInit()
  {
    console.log("parent content Initialized");
  }
  ngAfterContentChecked()
  {
    console.log("paret content checked");
  }
  // @ViewChild(ChildComponent) child;
  ngAfterViewInit()
  {
    // console.log(this.child.myname);
    // console.log(this.child.data);
    console.log("parent viewed");
  }
  ngAfterViewChecked()
  {
    console.log("parent view checked");
  }
  ngOnDestroy()
  {
    console.log("parent destroyed")
  }
}

