import { Component, EventEmitter, Input, Output,OnInit,OnChanges, SimpleChanges} from '@angular/core';


@Component({
  selector: 'app-child',
  // templateUrl: './child.component.html',
  styleUrls: ['./child.component.css'],
  template:
  `<p>{{data}} <p>
  <button (click)="childtoparent()">click me</button>`
})
export class ChildComponent {
  @Input() data:string;
  @Output() childEvent=new EventEmitter();
  myname="chandana";
  childtoparent()
  {
    this.childEvent.emit("child message!")
  }
  constructor()
  {}
  ngOnChanges()
  {
    console.log("child changed");
  }
  ngOnInit():void
  {
    console.log("child initialization");
  }
  ngDoCheck()
  {
    console.log("child checked");
  }
  ngAfterContentInit()
  {
    console.log("child content Initialized");
  }
  ngAfterContentChecked()
  {
    console.log("child content checked");
  }
  ngAfterViewInit()
  {
    console.log("child viewed");
  }
  ngAfterViewChecked()
  {
    console.log("child view checked");
  }
  ngOnDestroy()
  {
    console.log("child destroyed")
  }
}
