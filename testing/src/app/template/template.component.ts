import { Component, OnInit } from '@angular/core';
import { ITestTemplate } from '../itest-template';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
  testTemplate:ITestTemplate[]=[];
  testTemplates:ITestTemplate=<ITestTemplate>{};
  templates=[];
  json:string;
  constructor() { }

  ngOnInit(): void 
  {
    this.add();
  }
  add()
  {
    this.testTemplates=<ITestTemplate>{};
    this.testTemplates.dropDown=null;
    this.templates.push(this.templates.length);
    this.testTemplate.push(this.testTemplates);
  }
  submitTemplate()
  {
    // let json =this.testTemplate;
    let body =JSON.stringify(this.testTemplate);
    this.json =body;
  }
}

