import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location} from '@angular/common'
@Component({
  selector: 'app-third',
  templateUrl: './third.component.html',
  styleUrls: ['./third.component.css']
})
export class ThirdComponent implements OnInit {

  constructor(private location:Location,private router:Router) { }

  ngOnInit(): void {
  }
  goback() 
  {
    if (window.history.length > 2) 
    {
      this.location.back()
    } 
    else 
    {
      this.router.navigate(['/'])
    }
  }

}
