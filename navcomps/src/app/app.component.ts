import { PlatformLocation } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'navcomps';
  constructor(location: PlatformLocation, private router: Router) 
  {
    // location.onPopState(() => 
    // {
    // console.log('do u want to exit?');
    // //this.router.navigateByUrl(‘/multicomponent’);
    // //history.forward();
    // this.router.navigate(['/'])
    // });
    window.addEventListener("beforeunload", function(event) {
      event.returnValue=''
    })
  }
}
