import { componentFactoryName} from '@angular/compiler';
import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AppComponent } from './app.component'; 
import { DetailsComponent } from './details/details.component';
import { LoginComponent } from './login/login.component';
@Injectable({
  providedIn: 'root'
})
export class DeactivateGuard implements CanDeactivate <LoginComponent>{
  canDeactivate(component: LoginComponent) {
    if (component.loginForm.dirty) {
      return window.confirm('some content is unsaved!!');
    }
    return true;
  }
}

