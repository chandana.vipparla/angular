import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { DeactivateGuard } from './deactivate.guard';
import { DetailsComponent } from './details/details.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = 
[
  { path : 'loginForm',component :LoginComponent ,canDeactivate:[DeactivateGuard]},
  { path : 'details' ,component :DetailsComponent},
  { path :'admin' , component:AdminComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
