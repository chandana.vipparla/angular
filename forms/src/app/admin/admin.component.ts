import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  loginForm: FormGroup;
  constructor(private fb: FormBuilder) 
  {
    window.onload=()=>{
      confirm('there are some unsaved changes?')
    }
  }
  ngOnInit() {
      this.loginForm = this.fb.group({
          email: ['', [Validators.required, Validators.minLength(4)]],
          password: ['', [Validators.required, Validators.maxLength(8)]], 
      })
    }
  loginUser() {
      console.log(this.loginForm.status);
      console.log(this.loginForm.value);
  }
  cancel()
  {
    window.confirm("there are some unsaved changes?");
  }

}
