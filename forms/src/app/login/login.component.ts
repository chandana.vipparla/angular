import { getLocaleDayNames } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
    constructor(private fb: FormBuilder) 
    {
      // window.onload=()=>{
      //   confirm('there are some unsaved changes?')
      // }
      window.addEventListener("beforeunload", function(event) {
        event.returnValue=''
      })
    }
    ngOnInit() {
        this.loginForm = this.fb.group({
            email: ['', [Validators.required, Validators.minLength(4)]],
            password: ['', [Validators.required, Validators.maxLength(8)]], 
        })
      }
    loginUser() {
        console.log(this.loginForm.status);
        console.log(this.loginForm.value);
    }
    cancel()
    {
      window.confirm("there are some unsaved changes?");
    }
}
