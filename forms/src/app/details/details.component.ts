import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  details!: FormGroup;
  AppComponent: any;
    constructor(private fb: FormBuilder) 
    {
    }
    ngOnInit() {
        this.details = this.fb.group({
            firstname: ['', [Validators.required, Validators.minLength(4)]],
            lastname: ['', [Validators.required, Validators.maxLength(8)]]

        })
    }
    loginUser() {
        console.log(this.details.status);
        console.log(this.details.value);
    }

}
